//
//  FoldModel.swift
//  FoldTableView
//
//  Created by Mjwon on 2017/7/13.
//  Copyright © 2017年 Nemo. All rights reserved.
//

import UIKit

class FoldModel: NSObject {

    public var bgColor:UIColor?;
    public var titleColor:UIColor?;
    public var imageName:String?;
    public var title:String?;
    public var state:Bool?;
    public var descrip:String?;
    
}
